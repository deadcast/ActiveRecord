describe ActiveRecord::Base do
  class TestOne < ActiveRecord::Base
    attr_accessor :after_save_called, :before_destroy_called
    
    def after_save
      @after_save_called ||= 0
      @after_save_called += 1
    end
    
    def before_destroy
      @before_destroy_called ||= 0
      @before_destroy_called += 1
    end
  end
  
  class TestTwo < ActiveRecord::Base
  end
  
  before do
    TestOne.delete_all
    TestTwo.delete_all
  end
  
  describe '#new_record?' do
    describe 'the record has not been saved' do
      it 'returns true' do
        record = TestOne.new_record
        record.new_record?.should == true
      end
    end

    describe 'the record has been saved' do
      it 'returns false' do
        record = TestOne.new_record
        record.save
        record.new_record?.should == false
      end
    end
  end

  describe '#destroy' do
    it 'can delete the record' do
      record = TestOne.new_record
      record.save
      record.destroy
      TestOne.all.should == []
    end
  end

  describe '.new_record' do
    it 'returns back a record' do
      record = TestOne.new_record
      record.name.should == nil
    end
    
    it 'returns back a record with defaults' do
      record = TestOne.new_record({'name' => 'chip'})
      record.name.should == 'chip'
    end

    it 'does not have a context' do
      record = TestOne.new_record
      record.managedObjectContext.should == ActiveRecord::Store.context
    end
  end
   
  describe '.all' do
    it 'returns all records for the entity' do
      3.times { TestOne.new_record.save }
      TestOne.all.size.should == 3
      TestOne.all.each do |record|
        record.class.to_s.should == 'TestOne_TestOne_'
      end
    end
  end
  
  describe '.where' do
    it 'returns back a filtered array of records' do
      2.times { |i| TestOne.new_record('name' => 'coke', 'age' => i).save }
      TestOne.where({:name => 'coke'}).map(&:name).should == %w(coke coke)
      TestOne.where({:name => 'coke', :age => 1}).map(&:age).should == [1]
    end
  end
  
  describe '.order' do
    it 'returns results in ascending order' do
      2.times { |i| TestOne.new_record('name' => 'coke', 'age' => i).save }
      TestOne.order({:age => :ascending}).allObjects.map(&:age).should == [0, 1]
    end
    
    it 'returns results in descending order' do
      2.times { |i| TestOne.new_record('name' => 'coke', 'age' => i).save }
      TestOne.order({:age => :descending}).allObjects.map(&:age).should == [1, 0]
    end
  end
  
  describe '.sql' do
    it 'returns back a result from a query' do
      TestOne.new_record('name' => 'kitty', 'age' => 2).save
      TestOne.sql("name = 'kitty'").should == TestOne.where({:name => 'kitty'})
    end
  end
  
  describe '.delete_all' do
    it 'deletes all of the records' do
      TestOne.new_record.save
      TestOne.all.should.not == []
      TestOne.delete_all
      TestOne.all.should == []
    end
  end

  describe '#save' do
    it 'saves the record' do
      record = TestOne.new_record
      record.save
      record.objectID.persistentStore.should != nil
    end
  end
  
  describe '#after_save' do
    it 'calls after_save' do
      record = TestOne.new_record
      record.after_save_called.should == nil
      record.save
      record.after_save_called.should == 1
    end
    
    describe '#after_save is not defined' do
      it 'does not raise an error' do
        lambda do
          record = TestTwo.new_record({'name' => 'Billy'})
          record.save
        end.should.not.raise
      end
    end
  end
  
  describe '#before_destroy' do
    it 'calls before_destroy' do
      record = TestOne.new_record
      record.before_destroy_called.should == nil
      record.save
      record.destroy
      record.before_destroy_called.should == 1
    end
    
    describe '#before_destroy is not defined' do
      it 'does not raise an error' do
        lambda do
          record = TestTwo.new_record({'name' => 'Billy'})
          record.save
          record.destroy
        end.should.not.raise
      end
    end
    
    it 'does not call after_save' do
      record = TestOne.new_record
      record.after_save_called.should == nil
      record.before_destroy_called.should == nil
      record.save
      record.destroy
      record.after_save_called.should == 1
      record.before_destroy_called.should == 1
    end
  end
  
  describe '.field' do
    it 'creates a valid entity' do
      record = TestOne.new_record
      record.save
      record.respond_to?(:name).should == true
      record.new_record?.should == false
    end

    describe 'optional not set' do
      it 'defaults to false' do
        record = TestTwo.new_record
        lambda { record.save }.should.raise
        record.name = 'Kirby'
        lambda { record.save }.should.not.raise
      end
    end
  end
  
  describe 'relationships' do
    describe '.belongs_to' do
      it 'can belong to a model' do
        lambda do
          record_1 = TestOne.new_record({'name' => 'bob'})
          record_2 = TestTwo.new_record({'name' => 'mary'})
          record_1.test_two = record_2
          record_2.save
          record_1.save
        end.should.not.raise
      end
    end
    
    describe '.has_many' do
      it 'can have many objects' do
        record_1 = TestOne.new_record({'name' => 'steve'})
        record_2 = TestOne.new_record({'name' => 'joe'})
        record_3 = TestTwo.new_record({'name' => 'ashley'})
        record_3.save
        
        record_2.test_two = record_3
        record_2.save
        
        record_1.test_two = record_3
        record_1.save
        
        record_3.test_ones.count.should == 2
      end
    end
  end
  
  describe '#build_predicate_query' do
    it 'builds a query string' do
      query = ActiveRecord::Base.send(:build_predicate_query, {:name => 'john'})
      query.should == "name='john'"
    end
    
    it 'builds a query string with multiple attributes' do
      query = ActiveRecord::Base.send(:build_predicate_query, {:name => 'john', :age => '4'})
      query.should == "name='john' and age='4'"
    end
  end
  
  describe '.load_with_uri' do
    it 'returns back a test object' do
      test = TestOne.new_record({'name' => 'chip'})
      test.save
      uri = test.objectID.URIRepresentation.absoluteString
      TestOne.load_with_uri(uri).should == test
    end
    
    it 'returns nil when not found' do
      TestOne.load_with_uri('x-coredata:///TestOne/tE3EFD99F-5293-1234-8915-AE05325B592B235').should == nil
    end
  end
end