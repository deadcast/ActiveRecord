//
//  main.m
//  data_model
//
//  Created by Caleb Cohoon on 7/29/12.
//  Copyright (c) 2012 Caleb Cohoon. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
