ActiveRecord
=============

These classes were extracted out of a real life iOS app that I built for a client. I wrote this because I wanted to have some of those awesome Active Record helpers that I'm used to in Rails.

This implementation is an interesting one since it's a hybrid. You define some of your model in Ruby code and then you setup the rest like relationships and versioning in xcode. I like this much better since I don't have to implement a lot of features that xcode already gives you for free using their data modeling tool.

To understand how to actually use this, checkout the specs and the very simple data model file.

Thanks